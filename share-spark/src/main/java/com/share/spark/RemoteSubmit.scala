package com.share.spark

import org.apache.spark.{SparkConf, SparkContext}

object RemoteSubmit {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf()
      .setAppName("RemoteSubmitApp")
      .setMaster("spark://node18:7077")
      .setJars(List("/Users/mac/code/gitee/share-scala-maven/share-spark/target/share-spark-1.0-SNAPSHOT-single.jar"))
    //.setIfMissing("spark.driver.host", "node18")

    val sc = new SparkContext(conf)
    val lines = sc.parallelize(Array("aa", "bb", "cc", "aa"))
    val result = lines.
      flatMap(_.split(" "))
      .map((_, 1))
      .reduceByKey(_ + _, 1)
      .sortBy(_._2, false)

    result.collect.foreach(println _)

  }

}
