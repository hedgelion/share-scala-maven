
```text
https://www.jianshu.com/p/279bcf76039b?utm_campaign
```

mvn clean package -T 1C -Dmaven.test.skip=true -Dmaven.compile.fork=true

hdfs dfs -mkdir -p /data/spark
echo -e "11\n22 22\n33 333 3" > /tmp/wordcount.txt
cat /tmp/wordcount.txt
hdfs dfs -put -f /tmp/wordcount.txt /data/spark/wordcount.txt
hdfs dfs -cat hdfs://node17:8020/data/spark/wordcount.txt
hdfs dfs -rm -r /data/spark/output

# 提交任务到本地 当前主机 单线程
spark-submit --master local /opt/share-spark-1.0-SNAPSHOT-single.jar

# 提交任务到本地 当前主机 多进程多线程
spark-submit --master local-cluster[2,3,1024] /opt/share-spark-1.0-SNAPSHOT-single.jar 

# 提交任务到 yarn
spark-submit \
    --class com.share.spark.WordCount \
    --master spark://node18:7077 \  #  --master yarn
    --deploy-mode cluster \  # --deploy-mode client
    --driver-memory 2512m \
    --executor-memory 1g \
    --executor-cores 2 \
    --num-executors 3 \
    /opt/share-spark-1.0-SNAPSHOT-single.jar 


```text
注意 
1.spark 提交任务前要删除输出目录,同mapreduce任务
2.spark默认是读取或写入hdfs文件，读取或写入本地文件需要指定文件协议头file
```



https://blog.csdn.net/qq_40304825/article/details/92604850?utm_medium=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-1.nonecase&depth_1-utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-1.nonecase



