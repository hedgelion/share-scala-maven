package com.share.spark.es.reader

import org.apache.spark.{SparkConf, SparkContext}
import org.elasticsearch.spark._

object SparkEs {
  def main(args: Array[String]): Unit = {

    val conf = new SparkConf().setAppName("SparkEsReader").setMaster("local[*]")
    conf.set("es.index.auto.create", "true")
    conf.set("es.nodes", "node18")
    conf.set("es.port", "9200")

    val sc = new SparkContext(conf)
    val query: String =
      s"""{
              "query": {
                  "match_all": {}
              }
          }
      """
    val rdd = sc.esRDD("spark/docs", query)
    rdd.collect().foreach(println)
    println(rdd.count() + " -----------")
    sc.stop()
  }
}






