package com.share.spark.es.writer

import org.apache.spark.{SparkConf, SparkContext}
import org.elasticsearch.spark.rdd.EsSpark

object SparkEs {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("SparkEsWriter").setMaster("local[*]")
    conf.set("es.index.auto.create", "true")
    conf.set("es.nodes", "node18")
    conf.set("es.port", "9200")

    val sc = new SparkContext(conf)

    val numbers = Map("one" -> 1, "two" -> 2, "three" -> 3)
    val airports = Map("OTP" -> "Otopeni", "SFO" -> "San Fran")
    var rdd = sc.makeRDD(Seq(numbers, airports))
    EsSpark.saveToEs(rdd, "spark/docs")
    println("--------------------End-----------------")
  }
}
