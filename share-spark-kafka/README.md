
```text
https://www.jianshu.com/p/30614ff250b5
https://www.jianshu.com/p/1d41174441b6
```

mvn clean package -T 1C -Dmaven.test.skip=true -Dmaven.compile.fork=true

bin/kafka-server-start.sh -daemon /opt/kafka/config/server.properties  
# kafka-server-start.sh /opt/kafka/config/server.properties 

kafka-topics.sh \
    --create \
    --zookeeper node17:2181,node18:2181,node19:2181 \
    --topic sparktopic \
    --partitions 3 \
    --replication-factor 1

kafka-topics.sh \
    --list \
    --zookeeper node17:2181,node18:2181,node19:2181

kafka-topics.sh \
    --describe \
    --zookeeper node17:2181,node18:2181,node19:2181

# 提交任务到本地 当前主机 单线程
spark-submit \
    --master local \
    /opt/share-spark-kafka-1.0-SNAPSHOT.jar \
        ScalaKafkaStream \
        local[2] \
        /data/kafka/checkpoint \
        node18:9092 \
        kafka-test-group \
        sparktopic 

hdfs dfs -ls /data/kafka/checkpoint 

kafka-console-producer.sh  \
    --broker-list node18:9092 \
    --topic sparktopic  
    
kafka-console-consumer.sh \
    --bootstrap-server node18:9092 \
    --from-beginning \
    --group kafka-test-group \
    --topic sparktopic


