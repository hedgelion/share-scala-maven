
# 提交任务到本地 当前主机 单线程
spark-submit \
    --master local \
    /opt/share-kafka-spark-es-1.0-SNAPSHOT.jar \
        ScalaKafkaStream \
        local[2] \
        /data/kafka/checkpoint \
        node18:9092 \
        kafka-test-group \
        sparktopic \
        node18
