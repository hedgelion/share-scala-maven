package com.share.spark

import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.streaming.kafka010.{ConsumerStrategies, KafkaUtils, LocationStrategies}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}
import org.elasticsearch.spark.rdd.EsSpark

object SparkKafkaEs {
  def main(args: Array[String]): Unit = {
    println("--------------------Start-----------------")
    // 从kafka读取数据
    val appName = args(0)
    val master = args(1)
    val checkpoint = args(2)
    val bootstrapServers = args(3)
    val groupId = args(4)
    val topicName = args(5)
    val esNodes = args(6)

    // 设置spark conf
    val conf = new SparkConf()
      .setAppName(appName)
      .setMaster(master)
      // es 参数
      .set("es.index.auto.create", "true")
      .set("es.nodes", esNodes)
      .set("es.port", "9200")

    // 设置日志级别
    val sc = new SparkContext(conf)
    sc.setLogLevel("INFO")

    // 设置offset存储路径
    val ssc = new StreamingContext(sc, Seconds(5))
    ssc.checkpoint(checkpoint)

    // kafka 参数
    val kafkaParams = Map(
      ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG -> bootstrapServers,
      ConsumerConfig.GROUP_ID_CONFIG -> groupId,
      ConsumerConfig.MAX_POLL_RECORDS_CONFIG -> "500",
      ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG -> classOf[StringDeserializer],
      ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG -> classOf[StringDeserializer]
    )

    // kafka topic
    val kafkaTopicDS = KafkaUtils.createDirectStream(ssc, LocationStrategies.PreferConsistent,
      ConsumerStrategies.Subscribe[String, String](Set(topicName), kafkaParams))

    val kafkaStream = kafkaTopicDS.map(_.value)
      .flatMap(_.split(" "))
      .map(x => (x, 1L))
      .reduceByKey(_ + _)
      .transform(data => {
        val sortData = data.sortBy(_._2, false)
        sortData
      })

    // 将从kafka读取到的数据写入es
    //    kafkaStream.foreachRDD(rdd => {
    //      try {
    //        if (!rdd.isEmpty()) {
    //          println("write...")
    //          EsSpark.saveToEs(rdd, "spark/docs")
    //        } else {
    //          println("empty...")
    //        }
    //      } catch {
    //        case e: Throwable =>
    //          System.out.println("Error" + e.printStackTrace())
    //      }
    //    })

    val lines = kafkaStream.map(_._2)
    lines.foreachRDD(rdd => {
      if (!rdd.isEmpty()) {
        println("write...")
        val esRdd = rdd.map(line => {
          Map("mykey" -> line, "mycount" -> 1)
        })
        EsSpark.saveToEs(esRdd, "spark/docs")
      } else {
        println("empty...")
      }
    })

    ssc.start()
    ssc.awaitTermination()
    println("--------------------End-----------------")
  }

}